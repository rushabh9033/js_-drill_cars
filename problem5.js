const problem5 = (input, item) => {
    let model = []
    for (let i = 0; i < input.length; i++)
        if (input[i] < item) 
            model.push(input[i]);
    return model;
}

module.exports = problem5