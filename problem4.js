const problem4 = (input, item) => {
    let model = []
    for (let i = 0; i < input.length; i++) 
        model.push(input[i][item]);
    return model;
}

module.exports = problem4;