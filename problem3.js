const problem3 = (a,b) => {
    let car_nameA = a.car_model.toUpperCase();
    let car_nameB = b.car_model.toUpperCase();
    if (car_nameA < car_nameB) {
        return -1
    }
    if (car_nameA > car_nameB) {
        return 1;
    }
    return 0;
}


module.exports = problem3;
